### EatOut Kenya Coding Challenge ###

Consider the three text documents conveniently named one,txt, two.txt and three.txt found in this repository.

Write some code that looks through each document and gives each word a ranking based on the number of times it occurs in the text document.

The report from this part of the exercise should look a little like this:

```

{
	ranking: {
		word1: 93,
		word2: 56,
		etc..
	}

}

```

The above is just an example and you are encouraged to use better data structures.  The 93 and 56 in this case are the number of times word1 and word2 occur.

Once the above is done for each of the three text documents, produce an additional report that ranks each word based on the number of times it appears in all three documents.

##### Notes #####

* We are looking for clean, simple, testable, and well organized code written in the language you’re most comfortable with.

* You should provide sufficient evidence that your solution is complete through testing, as a minimum you should indicate that it works correctly against the provided dataset.

* Solutions should be completed and a link emailed to us within 3 days.  Please only send links to your code repository on Github, Bitbucket or similar sites.

* In case of any questions email support@eatout.co.ke